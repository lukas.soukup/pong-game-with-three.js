import * as THREE from 'https://cdn.jsdelivr.net/npm/three@0.118/build/three.module.js';
import {OrbitControls} from 'https://cdn.jsdelivr.net/npm/three@0.118/examples/jsm/controls/OrbitControls.js';

class Pong3D {
  stats;
	camera;
  controls;
  scene;
  ball;
  renderer;
  light;
  reverseMoveX;
  reverseMoveZ;
  playerOneBox;
  playerTwoBox;
  playerOneMovesLeft;
  playerOneMovesRight;
  playerTwoMovesLeft;
  playerTwoMovesRight;
  hitBall_sound;
  backgroundMusic;

  bot_plays = true;
  light_x = 30;
  light_y = 30;
  ballAngle = 90;
  BALL_SPEED = 2;
  ACCELERATION = 2;
  PLAYER_WIDTH = 25;
  PLAYER_SPEED = 1;
  SUN_SPEED = 0.00075;
  camera_y = 800;
  music_volume = 0.1;
  GAME_BORDERS = {x: 52.5, top: 100, bottom: 0, z: 102.5};

    constructor() {
        this.init();
    }

    init() {
            this.renderer = new THREE.WebGLRenderer();
            this.renderer.shadowMap.enabled = true;
            this.renderer.antialias = true;
            this.renderer.shadowMap.type = THREE.PCFSoftShadowMap;
            this.renderer.setPixelRatio(window.devicePixelRatio);
            this.renderer.setSize(window.innerWidth, window.innerHeight);

            document.body.appendChild(this.renderer.domElement);

            window.addEventListener('resize', () => {
                this.onWindowResize();
            }, false);

            // Display statistics of drawing to canvas
            this.stats = new Stats();
            this.stats.domElement.style.position = 'absolute';
            this.stats.domElement.style.top = '0px';
            this.stats.domElement.style.zIndex = 100;
            document.body.appendChild( this.stats.domElement );
            // red axis = X
            // green axis = Y
            // blue axis = Z

            this.sceneSetup();
            this.camera_and_Controls_Handeler();
            this.addLights();
            this.addObjects();
            this.addAudio();
            this._RAF();
        }

        _RAF() {
            requestAnimationFrame(() => {
                    this.renderer.render(this.scene, this.camera);
                    this._RAF();
                    this.stats.update();
            });
            //this.controls.target = this.ball.position;
            this.moveBall();
            this.movePlayer();
            this.moveBot();
            //console.log(this.playerOneBox.position);
            this.controls.update();
            if(this.camera_y > 60) {
              this.camera.position.y = this.camera_y;
              this.camera_y -= 3;
            }
            this.light.position.x = Math.sin(this.light_x)*300;
            this.light.position.y = Math.cos(this.light_y)*300;
            this.light_x += this.SUN_SPEED;
            this.light_y += this.SUN_SPEED;
        }

        sceneSetup() {
          this.scene = new THREE.Scene();
          //const axesHelper = new THREE.AxesHelper( 50 );
          //this.scene.add( axesHelper );
          const loader = new THREE.CubeTextureLoader();
          const texture = loader.load([
              "../textures/skybox_posX.png",
              "../textures/skybox_negX.png",
              "../textures/skybox_posY.png",
              "../textures/skybox_negY.png",
              "../textures/skybox_posZ.png",
              "../textures/skybox_negZ.png",
          ]);
          this.scene.background = texture;
        }

        
        camera_and_Controls_Handeler() {
          const fov = 60;
          const aspect = 1920 / 1080;
          const near = 1.0;
          const far = 1000.0;

          this.camera = new THREE.PerspectiveCamera(fov, aspect, near, far);
          this.camera.position.set(-20, 60, -150);

          this.controls = new OrbitControls(this.camera, this.renderer.domElement);
          this.controls.keys = {
            LEFT: null,
            UP: 'ArrowUp', // up arrow
            RIGHT: null, // right arrow
            BOTTOM: 'ArrowDown' // down arrow
          }
          this.controls.maxPolarAngle = Math.PI/2.2;
          this.controls.enableDamping = true;
          this.controls.update();
        }

        addAudio() {
          const listener = new THREE.AudioListener();
          this.camera.add( listener );
          const sound = new THREE.Audio( listener );
          const audioLoader = new THREE.AudioLoader();
          audioLoader.load( 'sounds/HitBall.m4a', function( buffer ) {
            sound.setBuffer( buffer );
            sound.setVolume( 1 );
            sound.hasPlaybackControl = true;
          });
          this.hitBall_sound = sound;
          this.hitBall_sound.onended = function() {
            sound.stop();
          }
          const sound2 = new THREE.Audio( listener );
          const defaultVolume = this.music_volume;
          audioLoader.load( 'sounds/yoitrax-warrior.mp3', function( buffer ) {
            sound2.setBuffer( buffer );
            sound2.setLoop( true );
            sound2.setVolume( defaultVolume );
          });
          this.backgroundMusic = sound2;
        }

        addLights() {
          this.light = new THREE.DirectionalLight(0xfdf3c6);
          this.light.position.set(-300, 10, 50);
          this.light.target.position.set(0, 0, 0);
          this.light.castShadow = true;
          this.light.shadow.bias = -0.01;
          this.light.shadow.mapSize.width = 2048;
          this.light.shadow.mapSize.height = 2048;
          this.light.shadow.camera.near = 1.0;
          this.light.shadow.camera.far = 500;
          this.light.shadow.camera.left = 200;
          this.light.shadow.camera.right = -200;
          this.light.shadow.camera.top = 200;
          this.light.shadow.camera.bottom = -200;
          this.light.name = "DirectionalLight";
          this.scene.fog = new THREE.FogExp2(0xEFEFEB, 0.0025);
          //const helper = new THREE.DirectionalLightHelper( this.light, 5 );
          //this.scene.add(helper);
          this.scene.add(this.light);

          var light = new THREE.AmbientLight(0x404040);
          light.name = "AmbientLight";
          this.scene.add( light );
        }

        addObjects() {
          const terrainLoader = new THREE.TextureLoader();
          const terrainTexture = terrainLoader.load( '../textures/terrainTexture.jpg');
          const terrain = new THREE.Mesh(
              new THREE.PlaneGeometry(8688/2, 5792/2),
              new THREE.MeshStandardMaterial({
                  map: terrainTexture,
              }));
          terrain.castShadow = true;
          terrain.receiveShadow = true;
          terrain.position.y = this.GAME_BORDERS.bottom-0.1;
          terrain.rotation.x = -Math.PI / 2;
          terrain.fog = true;
          this.scene.add( terrain );

          // GAME FLOOR
          const plane = new THREE.Mesh(
              new THREE.PlaneGeometry(100, 200),
              new THREE.MeshStandardMaterial({
                  color: 0xFFFFFF,
              }));
          plane.castShadow = true;
          plane.receiveShadow = true;
          plane.material.side = THREE.DoubleSide;
          plane.position.y = this.GAME_BORDERS.bottom;
          plane.rotation.x = -Math.PI / 2;
          plane.name = "Border-y"
          this.scene.add( plane );
          
          // GAME BORDERS
          const fence_geometry = new THREE.BoxGeometry( 110, 10, 5 );
          const fence_material = new THREE.MeshPhongMaterial( {color: 0x595959} );
          const fence1 = new THREE.Mesh( fence_geometry, fence_material );
          fence1.receiveShadow = true;
          fence1.castShadow = true;
          fence1.position.y = 5;
          fence1.position.z = this.GAME_BORDERS.z;
          fence1.name = "Border+z";
          const fence2 = fence1.clone();
          fence2.position.z = -this.GAME_BORDERS.z;
          fence2.name = "Border-z";
          this.scene.add( fence1 );
          this.scene.add( fence2 );

          const fence3_geometry = new THREE.BoxGeometry( 5, 10, 200 );
          const fence3_material = new THREE.MeshPhongMaterial( {color: 0x595959} );
          const fence3 = new THREE.Mesh( fence3_geometry, fence3_material );
          fence3.receiveShadow = true;
          fence3.castShadow = true;
          fence3.position.y = 5;
          fence3.position.x = this.GAME_BORDERS.x;
          fence3.name = "Border+x";
          const fence4 = fence3.clone();
          fence4.position.x = -this.GAME_BORDERS.x;
          fence4.name = "Border-x";
          this.scene.add( fence3 );
          this.scene.add( fence4 );

          const midLine_geometry = new THREE.PlaneGeometry(100, 2.5);
          const midLine_material = new THREE.MeshBasicMaterial({ color: 0x272727 });
          const middleLine = new THREE.Mesh( midLine_geometry, midLine_material );
          middleLine.rotation.x = -Math.PI / 2;
          middleLine.position.y = 0.01;
          this.scene.add(middleLine);

          // BOUNCING BALL
          const ball_loader = new THREE.TextureLoader();
          const ball_texture = ball_loader.load( '../textures/tenisBall.png');
          const ball_geometry = new THREE.SphereGeometry(2, 100, 100);
          const ball_material = new THREE.MeshStandardMaterial({ map: ball_texture });
          const ball = new THREE.Mesh( ball_geometry, ball_material );
          ball.position.set(0,2,0);
          ball.castShadow = true;
          ball.overdraw = true;
          ball.receiveShadow = true;
          this.ball = ball;
          this.scene.add(this.ball);
          
          // PLAYER OBJECTS
          const playerOne_geometry = new THREE.BoxGeometry( this.PLAYER_WIDTH, 8, 2 );
          const playerOne_material = new THREE.MeshStandardMaterial( {color: 0x101010, roughness: 1.0} );
          const playerOneBox = new THREE.Mesh( playerOne_geometry, playerOne_material );
          playerOneBox.position.set( 0, 5, -85 );
          playerOneBox.castShadow = true;
          playerOneBox.overdraw = true;
          playerOneBox.receiveShadow = true
          this.playerOneBox = playerOneBox;
          this.scene.add(this.playerOneBox);
          this.playerTwoBox = playerOneBox.clone();
          this.playerTwoBox.position.z = 85;
          this.scene.add(this.playerTwoBox);
        }
        onWindowResize() {
          this.camera.aspect = window.innerWidth / window.innerHeight;
          this.camera.updateProjectionMatrix();
          this.renderer.setSize(window.innerWidth, window.innerHeight);
        }

        moveBall() {
          // split this into x and z components based on initial angle
          const xVel = Math.cos( this.ballAngle*Math.PI/180 ) * this.BALL_SPEED;
          const zVel = Math.sin( this.ballAngle*Math.PI/180 ) * this.BALL_SPEED;
          if(this.playerOneColision()) this.reverseMoveZ = !this.reverseMoveZ;
          if(this.playerTwoColision()) this.reverseMoveZ = !this.reverseMoveZ;
          if (this.ball.position.x >= this.GAME_BORDERS.x - 4.5) this.reverseMoveX = true;
          else if (this.ball.position.x <= (-this.GAME_BORDERS.x) + 4.5) this.reverseMoveX = false;
          if(this.reverseMoveX) {
            this.ball.position.x -= xVel;
            this.ball.rotateZ(-xVel/8);
          }
          else {
            this.ball.position.x += xVel;
            this.ball.rotateZ(xVel/8);
          }          
          if (this.ball.position.z >= this.GAME_BORDERS.z - 4.5 ) this.reverseMoveZ = true;
          else if (this.ball.position.z <= (-this.GAME_BORDERS.z) + 4.5 ) this.reverseMoveZ = false;
          if(this.reverseMoveZ) {
            this.ball.position.z -= zVel;
            this.ball.rotateX(-zVel/8);
          }
          else {
            this.ball.position.z += zVel;
            this.ball.rotateX(zVel/8);
          }
            this.BALL_SPEED *= 0.99166;
        }

        moveBot() {
          if(!this.bot_plays) return;
          if((this.playerTwoBox.position.x + this.PLAYER_WIDTH/2) < this.GAME_BORDERS.x - 3 
            && (this.playerTwoBox.position.x + this.PLAYER_WIDTH/4) <= this.ball.position.x) {
              this.playerTwoBox.position.x += this.PLAYER_SPEED;
           }
          else if((this.playerTwoBox.position.x - this.PLAYER_WIDTH/2) > -this.GAME_BORDERS.x + 3 
            && (this.playerTwoBox.position.x - this.PLAYER_WIDTH/4) >= this.ball.position.x) {
              this.playerTwoBox.position.x -= this.PLAYER_SPEED;
          }
        }

        movePlayer() {
          if(this.playerOneMovesLeft) {
            if((this.playerOneBox.position.x + this.PLAYER_WIDTH/2) < this.GAME_BORDERS.x - 3) {
              this.playerOneBox.position.x += this.PLAYER_SPEED;
            }
          }if(this.playerOneMovesRight) {
            if((this.playerOneBox.position.x - this.PLAYER_WIDTH/2) > -this.GAME_BORDERS.x + 3) {
              this.playerOneBox.position.x -= this.PLAYER_SPEED;
            }
          }
          if(this.playerTwoMovesLeft && !this.bot_plays) {
            if((this.playerTwoBox.position.x + this.PLAYER_WIDTH/2) < this.GAME_BORDERS.x - 3) {
              this.playerTwoBox.position.x += this.PLAYER_SPEED;
            }
          }if(this.playerTwoMovesRight && !this.bot_plays) {
            if((this.playerTwoBox.position.x - this.PLAYER_WIDTH/2) > -this.GAME_BORDERS.x + 3) {
              this.playerTwoBox.position.x -= this.PLAYER_SPEED;
            }
          }
        }

        playerOneColision() {
          if( this.ball.position.z <= this.playerOneBox.position.z + 2 && this.ball.position.z >= this.playerOneBox.position.z - 2
            && this.ball.position.x >= (this.playerOneBox.position.x - this.PLAYER_WIDTH/2) && this.ball.position.x <= (this.playerOneBox.position.x + this.PLAYER_WIDTH/2)) {
              this.BALL_SPEED += this.ACCELERATION;
              this.ballAngle = Math.random()*(90-30)+30;
              this.hitBall_sound.play();
              return true;
            }
              return false;
        }

        playerTwoColision() {
          if( this.ball.position.z >= this.playerTwoBox.position.z - 2 && this.ball.position.z <= this.playerTwoBox.position.z + 2 
            && this.ball.position.x >= (this.playerTwoBox.position.x - this.PLAYER_WIDTH/2) && this.ball.position.x <= (this.playerTwoBox.position.x + this.PLAYER_WIDTH/2)) {
              this.BALL_SPEED += this.ACCELERATION;
              this.ballAngle = Math.random()*(90-30)+30;
              this.hitBall_sound.play();
              return true;
            }
              return false;
        }
      }

    let app = null;
    const accelerate = 4;

window.addEventListener('DOMContentLoaded', () => {
  app = new Pong3D();
  document.getElementById("volume").value = app.music_volume;
  document.getElementById("playerSize").value = app.PLAYER_WIDTH;
  document.getElementById("playerSpeed").value = app.PLAYER_SPEED;
  document.getElementById("pvp_pve_switch").checked = app.bot_plays;
});

window.onkeyup = (event) => {
  keyBindings(event.keyCode, false);
};

window.onkeydown = (event) => {
  keyBindings(event.keyCode, true);
  if(!app.backgroundMusic.isPlaying) {
    app.backgroundMusic.play();
  }
};

document.getElementById("volume").oninput = () => {
  app.backgroundMusic.setVolume(document.getElementById("volume").value);
  if(!app.backgroundMusic.isPlaying) {
    app.backgroundMusic.play();
  }
}

document.getElementById("playerSize").oninput = () => {
  const val = document.getElementById("playerSize").value;
  app.PLAYER_WIDTH = val;
  app.playerOneBox.scale.x = val/25;
  app.playerTwoBox.scale.x = val/25;
}
document.getElementById("playerSpeed").oninput = () => {
  var val = document.getElementById("playerSpeed").value;
  val = parseFloat(val);
  console.log(val);
  app.PLAYER_SPEED = val;
}
document.getElementById("pvp_pve_switch").onchange = () => {
  app.bot_plays = document.getElementById("pvp_pve_switch").checked;
}
document.getElementById("help").onclick = () => {
  var controls = document.getElementById("controls");
  var handeler = document.getElementById("help");
  if(controls.style.left != "15px") {
    controls.style.left = "15px";
    handeler.innerHTML = "Close";
    handeler.style.backgroundColor = "rgba(211, 27, 27, 0.562)";
  }else {
    controls.style.left = "-195px";
    handeler.innerHTML = "Controls";
    handeler.style.backgroundColor = "rgba(128, 128, 128, 0.562)";
  }
}

document.getElementById("settings").onclick = () => {
  var settings = document.getElementById("slidecontainer");
  var handeler = document.getElementById("settings");
  if(settings.style.left != "10px") {
    settings.style.left = "10px";
    handeler.innerHTML = "Close";
    handeler.style.backgroundColor = "rgba(211, 27, 27, 0.562)";
  }else {
    settings.style.left = "-165px";
    handeler.innerHTML = "Settings";
    handeler.style.backgroundColor = "rgba(128, 128, 128, 0.562)";
  }
}

function keyBindings(keyCode, bool) {
  switch (keyCode) {
    case 13:
      app.bot_plays = true;
      document.getElementById("pvp_pve_switch").checked = app.bot_plays;
      break;
    case 8:
      app.bot_plays = false;
      document.getElementById("pvp_pve_switch").checked = app.bot_plays;
      break;
    case 32:
      app.BALL_SPEED = accelerate;
      break;
    case 65:// a
      app.playerOneMovesLeft = bool;
      break;
    case 37:// <-
      app.playerTwoMovesLeft = bool;
      break;  
    case 68:// d
      app.playerOneMovesRight = bool;
      break;
    case 39:// ->
      app.playerTwoMovesRight = bool;
      break;
    default:
      return;
  }
}