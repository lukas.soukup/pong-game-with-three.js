//var liveServer = require("live-server");
const express = require('express');
const app = express();
const morgan = require('morgan');
const path = require('path');

app.use(express.static(__dirname));
app.get('/', function(req, res) {
    res.sendFile('cv07/index.html', { root: path.join(__dirname, "cv07") });
});
app.use(morgan('dev'));

//course error handeling 
//umoznuje odesilat req z SPA, atd...
app.use((req, res, next) => {
    res.header("Access-Control-Allow-Origin", "*"); //umoznuje pristup pro vsechny clienty
    if (req.method === "OPTIONS") {
        res.header("Access-Control-Allow-Methods", "PUT, GET, POST, PATCH, DELETE");
        return res.status(200).json({});
    }
    next(); //pro preposlani do dalsiho routeru
});

//middleware ktery odchytava requesty mirene na neexistujci odkaz
app.use((req, res, next) => {
    const error = new Error('Not Found');
    error.status = 404;
    next(error);
});

//middleware ktery odchytava vsechny ostatni errory
app.use((error, req, res, next) => {
    res.status(error.status || 500);
    res.json({
        error: {
            message: error.message
        }
    });
});

const http = require('http');
const port = process.env.PORT || 8080;
const server = http.createServer(app);
server.listen(port, () => console.log("Server running at port " + port)); 


//var params = {
//    port: 8080, // Set the server port. Defaults to 8080.
//    host: "0.0.0.0", // Set the address to bind to. Defaults to 0.0.0.0 or process.env.IP.
//    root: "D://Programovaci aplikace//VSC programming//Pocitacova_grafika//cv07", // Set root directory that's being served. Defaults to cwd.
//    open: true, // When false, it won't load your browser by default.
//    ignore: 'scss,my/templates', // comma-separated string for paths to ignore
//    file: "index.html", // When set, serve this file (server root relative) for every 404 (useful for single-page applications)
//    wait: 1000, // Waits for all changes, before reloading. Defaults to 0 sec.
//    mount: [['/components', './node_modules']], // Mount a directory to a route.
//    logLevel: 2, // 0 = errors only, 1 = some, 2 = lots
//    middleware: [function(req, res, next) { next(); }] // Takes an array of Connect-compatible middleware that are injected into the server middleware stack
//};
//liveServer.start(params);